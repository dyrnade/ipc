# DFL IPC

Two very simply classes for IPC, especially between two instances of the same application. These classes are used in DFL::Application.


### Dependencies:
* <tt>Qt5 Core and Gui (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/ipc.git df5ipc`
- Enter the `df5ipc` folder
  * `cd df5ipc`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.2
* Meson:            0.60.1
* Ninja:            1.10.1

### Known Bugs
* Please test and let us know


### Upcoming
* A nice way to handle unix signals like SIGINT, SIGTERM, SIGHUP, etc from within DesQ*Application (#1)
* Pickup the default font from font-config (#3)
