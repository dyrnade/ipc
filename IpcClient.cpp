/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#include "IpcClient.hpp"
#include "IpcClientImpl.hpp"

// C STL
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>
#include <bits/stdc++.h>

DFL::IPC::Client::Client( QString sockPath, QObject *parent ) : QObject( parent ) {
    mSockPath = sockPath;

    impl = new DFL::IPC::ClientImpl();
    impl->setAutoDelete( false );
}


DFL::IPC::Client::~Client() {
    disconnectFromServer();

    impl->disconnect();
    delete impl;
}


bool DFL::IPC::Client::connectToServer() {
    /** We're connected to the server and have received a registration ID */
    if ( impl->mRegistered ) {
        return true;
    }

    /** SafetyNet: May be we're connected but yet to receive a registration ID */
    if ( impl->fdarr.fd > 0 ) {
        return true;
    }

    /** Create socket: Local socket, with two way byte-stream connection */
    mSockFD = socket( AF_LOCAL, SOCK_STREAM, 0 );

    /** We failed to create a socket */
    if ( mSockFD == -1 ) {
        qCritical( "Failed to create a socket: %s", strerror( errno ) );
        emit socketError( errno );

        return false;
    }

    struct sockaddr_un server;
    signal( SIGTSTP, SIG_IGN );

    /** Connect @mSockFD to the address */
    server.sun_family = AF_LOCAL;
    strcpy( server.sun_path, mSockPath.toUtf8().constData() );

    int ret = ::connect( mSockFD, (struct sockaddr *)&server, SUN_LEN( &server ) );

    if ( ret < 0 ) {
        qCritical( "Failed to connect to the server: %s", strerror( errno ) );
        emit serverNotRunning();

        return false;
    }

    else {
        qInfo() << "Connected to server." << mSockFD;
        emit connected();
    }

    impl->fdarr.fd     = mSockFD;
    impl->fdarr.events = POLLRDNORM;

    connect( impl, &DFL::IPC::ClientImpl::messageReceived, this, &DFL::IPC::Client::messageReceived );
    connect( impl, &DFL::IPC::ClientImpl::connected,       this, &DFL::IPC::Client::connected );

    connect(
        impl, &DFL::IPC::ClientImpl::disconnected, [ = ] () {
            close( mSockFD );
            mSockFD = -1;

            emit disconnected();
        }
    );

    QThreadPool::globalInstance()->start( impl );

    return true;
}


bool DFL::IPC::Client::waitForRegistered( qint64 timeout ) {
    qint64 time = 0;

    do {
        /** Low latency: Wait for 1 us */
        QThread::usleep( 1 );

        /** Bump the time count */
        time++;

        /** Don't let the */
        qApp->processEvents();

        /** If we've reached the time limit, quit the loop */
        if ( (timeout > 0) and (time >= timeout) ) {
            return false;
        }
    } while ( not impl->mRegistered );

    /** We're registered */
    return true;
}


bool DFL::IPC::Client::sendMessage( QString msg ) {
    /** Write data into the sockfd */
    int bytes = write( mSockFD, msg.toUtf8().constData(), msg.size() );

    /** Flush the fd */
    fsync( mSockFD );

    /** We can now start waiting for the reply */
    impl->mReplied = false;
    impl->mReply   = QString();

    if ( msg.size() == bytes ) {
        return true;
    }

    qWarning() << "Error writing message:" << strerror( errno );

    return false;
}


bool DFL::IPC::Client::waitForReply( qint64 timeout ) {
    qint64 time = 0;

    do {
        /** Low latency: Wait for 1 us */
        QThread::usleep( 1 );

        /** Bump the time count */
        time++;

        /** Don't let the */
        qApp->processEvents();

        /** If we've reached the time limit, quit the loop */
        if ( (timeout > 0) and (time >= timeout) ) {
            return false;
        }
    } while ( not impl->mReplied );

    /** We've received a reply. */
    return true;
}


QString DFL::IPC::Client::reply() {
    return impl->mReply;
}


void DFL::IPC::Client::disconnectFromServer() {
    if ( not impl->mTerminate ) {
        impl->mTerminate = true;
        /** Force QApplication to clear it's queued events */
        qApp->processEvents();
        /** 250 ms is plenty time to quit */
        bool ret = QThreadPool::globalInstance()->waitForDone( 250 );
        if ( not ret ) {
            qWarning() << "Unable to stop polling.";
        }
    }

    if ( mSockFD != -1 ) {
        close( mSockFD );
        mSockFD = -1;

        emit disconnected();
    }
}
