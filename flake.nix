{
  description = "A flake for building Hello World";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.default =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation rec {
  pname = "ipc";
  version = "v0.1.1";
  # https://gitlab.com/desktop-frameworks/ipc.git
  src = fetchFromGitLab {
    hash = "sha256-7tjO7s14YdvgiApgJr5SpmLVA3jz644nuVRNiJQi9k4=";
    domain = "gitlab.com";
    owner = "desktop-frameworks";
    repo = pname;
    rev = version;
  };
  outputs = [ "out" ];
  
  nativeBuildInputs = [
    ninja meson pkgconfig cmake python3 qt6.wrapQtAppsHook
  ];
  buildInputs = [
    qt6.qtbase
  ];

  mesonFlags = [ "--prefix=${placeholder "out"}/usr --buildtype=release  -Duse_qt_version=qt6" ];
      };

  };
}

