/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Server is unix domain socket based server, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Client to communicate between two instances of the same app.
 **/

#include "IpcServer.hpp"
#include "IpcServerImpl.hpp"

// C STL
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>
#include <bits/stdc++.h>

#define MAX_CONNS    128

DFL::IPC::Server::Server( QString path, QObject *parent ) : QObject( parent ) {
    mSockPath = path;

    impl = new ServerImpl();
    impl->setAutoDelete( false );
}


DFL::IPC::Server::~Server() {
    if ( impl->mSockFD ) {
        shutdown();
    }

    QFile::remove( mSockPath );
}


bool DFL::IPC::Server::startServer() {
    /** Create socket: Local socket, with two way byte-stream connection */
    impl->mSockFD = socket( AF_LOCAL, SOCK_STREAM, 0 );

    /** We failed to create a socket */
    if ( impl->mSockFD == -1 ) {
        qCritical( "Failed to create a socket: %s", strerror( errno ) );
        emit socketError( errno );

        return false;
    }

    /** Destroy if this path exists */
    unlink( mSockPath.toUtf8().constData() );

    struct sockaddr_un server;
    signal( SIGTSTP, SIG_IGN );

    /** Bind @mSockFD to the address */
    server.sun_family = AF_LOCAL;
    strcpy( server.sun_path, mSockPath.toUtf8().constData() );

    int ret = bind( impl->mSockFD, (struct sockaddr *)&server, SUN_LEN( &server ) );

    if ( ret < 0 ) {
        qCritical( "Failed to bind server address to it's F: %s", strerror( errno ) );
        emit socketError( errno );

        return false;
    }

    /** Listen to @mSockFD for incoming connections */
    listen( impl->mSockFD, MAX_CONNS );

    impl->fdarr[ 0 ].fd     = impl->mSockFD;
    impl->fdarr[ 0 ].events = POLLRDNORM;

    for ( int i = 1; i < MAX_CONNS; i++ ) {
        impl->fdarr[ i ].fd = -1;
    }

    connect( impl, &DFL::IPC::ServerImpl::messageReceived, this, &DFL::IPC::Server::messageReceived );
    connect( impl, &DFL::IPC::ServerImpl::disconnected,    this, &DFL::IPC::Server::disconnected );
    connect( impl, &DFL::IPC::ServerImpl::sendMessage,     this, &DFL::IPC::Server::reply );

    QThreadPool::globalInstance()->start( impl );

    return true;
}


void DFL::IPC::Server::broadcast( QString message ) {
    for ( int clientFD: impl->lobby.values() ) {
        reply( clientFD, message );
    }
}


bool DFL::IPC::Server::reply( int fd, QString msg ) {
    int ret = write( fd, msg.toUtf8().constData(), msg.size() );

    if ( ret < 0 ) {
        qCritical() << "Error writing to client:" << strerror( errno );
        return false;
    }

    return true;
}


void DFL::IPC::Server::shutdown() {
    impl->mTerminate = true;
    QThreadPool::globalInstance()->waitForDone();

    if ( impl->mSockFD != -1 ) {
        close( impl->mSockFD );
        impl->mSockFD = -1;

        emit disconnected();
    }
}
